import 'package:alcodes_on_board_flutter/screens/home_screen.dart';
import 'package:alcodes_on_board_flutter/screens/login_screen.dart';
import 'package:alcodes_on_board_flutter/screens/my_friend_details.dart';
import 'package:alcodes_on_board_flutter/screens/my_friend_list.dart';
import 'package:alcodes_on_board_flutter/screens/splash_screen.dart';
import 'package:alcodes_on_board_flutter/screens/term_screen.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static const splashScreen = '/';
  static const login = '/login';
  static const home = '/home';
  static const myFriendList = '/my_friend_list';
  static const myFriendDetails = '/my_friend_details';
  static const term = '/term';

  static Route<dynamic> generatedRoute(RouteSettings settings) =>
      _materialPageRoute(_getWidgetByRouteName(settings.name, settings));

  static Widget _getWidgetByRouteName(
      String routeName, RouteSettings settings) {
    Widget widget;

    switch (routeName) {
      case splashScreen:
        widget = SplashScreen();
        break;
      case login:
        widget = LoginScreen();
        break;
      case home:
        widget = HomeScreen();
        break;
      case myFriendList:
        widget = MyFriendList();
        break;
      case myFriendDetails:
        widget = MyFriendDetails(friendList: settings.arguments);
        break;
      case term:
        widget = TermScreen();
        break;

      default:
        throw UnimplementedError('Unimplemented route: $routeName');
    }

    return widget;
  }

  static MaterialPageRoute _materialPageRoute(Widget widget) =>
      MaterialPageRoute(builder: (_) => widget);
}
