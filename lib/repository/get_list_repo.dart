import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:alcodes_on_board_flutter/models/view_models/friend_list_model.dart';
import 'package:alcodes_on_board_flutter/utils/api_components/api_response.dart';
import 'package:dio/dio.dart';

class GetListRepo {
  Future<ApiResponse<List<FriendListModel>>> getUserListAsync(
      {String link}) async {
    try {
      List<FriendListModel> friendList = new List<FriendListModel>();
      List data;
      final dio = Dio();
      dio.options.baseUrl = ApiUrls.baseUrl;

      final response = await dio.get(ApiUrls.listUser + link);

      data = response.data["data"];

      data.forEach((friend) {
        friendList.add(FriendListModel.fromJson(friend));
      });

      return ApiResponse<List<FriendListModel>>(
        message: "OK",
        data: friendList,
      );
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }
}
