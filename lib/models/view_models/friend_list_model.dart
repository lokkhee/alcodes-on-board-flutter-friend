class FriendListModel {
  int id;
  String email;
  String firstName;
  String lastName;
  String avatar;

  FriendListModel(
      {this.id, this.email, this.firstName, this.lastName, this.avatar});

  factory FriendListModel.fromJson(Map<String, dynamic> json) {
    return FriendListModel(
      id: json["id"],
      email: json["email"],
      firstName: json["first_name"],
      lastName: json["last_name"],
      avatar: json["avatar"],
    );
  }
}
