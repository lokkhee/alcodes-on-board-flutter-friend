import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermScreen extends StatefulWidget {
  @override
  _TermScreenState createState() => _TermScreenState();
}

class _TermScreenState extends State<TermScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Terms of Use"),
        ),
        body: WebView(
          initialUrl: 'https://www.random.org/terms/2020-08-01/website',
          javascriptMode: JavascriptMode.unrestricted,
        ));
  }
}
