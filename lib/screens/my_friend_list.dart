import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/models/view_models/friend_list_model.dart';
import 'package:alcodes_on_board_flutter/repository/get_list_repo.dart';
import 'package:flutter/material.dart';

/// TODO Issue on this screen:
/// - Show list of my friends.
/// - Click list item go to my friend detail page.

class MyFriendList extends StatefulWidget {
  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {
  Future<List<FriendListModel>> _futureBuilder;
  List<FriendListModel> _friendList;

  @override
  void initState() {
    _futureBuilder = _getDataAsync();
    super.initState();
  }

  Future<List<FriendListModel>> _getDataAsync() async {
    final repo = GetListRepo();
    final response = await repo.getUserListAsync(link: "?page=2");
    _friendList = response.data;

    return _friendList;
  }

  void _deleteNote(int index) {
    setState(() {
      _friendList.removeAt(index);
    });
  }

  void _onTapFriendListItem(FriendListModel currentFriendDetails) {
    Navigator.of(context)
        .pushNamed(AppRouter.myFriendDetails, arguments: currentFriendDetails);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Friend List'),
        centerTitle: true,
      ),

      ///Check for data, if none, show No Records Found label
      ///If there are data, display the list view
      body: FutureBuilder<List<FriendListModel>>(
          initialData: null,
          future: _futureBuilder,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.hasData) {
              return _content(context);
            }
            return Center(child: const Text("No Records Found"));
          }),
    );
  }

  Widget _content(BuildContext context) {
    return ListView.builder(
      itemCount: _friendList.length,
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
        child: Card(
          child: ListTile(
            onTap: () {
              _onTapFriendListItem(
                  _friendList[index]); //Goes to the respective note
            },
            leading: Container(
                width: 40.0,
                height: 40.0,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                            _friendList[index].avatar.toString())))),
            title: Text(
                "${_friendList[index].firstName} ${_friendList[index].lastName}"),
            subtitle: Text(_friendList[index].email),
            trailing: FlatButton(
              child: Text("Delete"),
              onPressed: () {
                _deleteNote(index);
              },
            ),
          ),
        ),
      ),
    );
  }
}
