import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/form_models/sign_in_form_model.dart';
import 'package:alcodes_on_board_flutter/repository/auth_repo.dart';
import 'package:alcodes_on_board_flutter/utils/app_focus_helper.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen:
/// - Password field should hide the inputs for security purpose.
/// - Login button is too small, make it fit the form width.
/// - Should show loading when calling API, and hide loading when server responded.
/// - Wrong login credential is showing as Toast, Toast will auto dismiss
///   and user will miss out the error easily, should use popup dialog.

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formModel = SignInFormModel();
  String _buttonText = "Login";
  bool _isButtonDisabled;

  @override
  void initState() {
    super.initState();
    _isButtonDisabled = false;
  }

  void _onTextPressed() {
    Navigator.of(context).pushNamed(AppRouter.term);
    // Navigator.of(context).pushReplacementNamed(AppRouter.tern);
    // Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.term, (route) => false);
  }

  Future<bool> _check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return Future.value(true);
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return Future.value(true);
    }
    return Future.value(false);
  }

  Future<void> _onSubmitFormAsync() async {
    bool connection = true;
    // Hide keyboard.
    AppFocusHelper.instance.requestUnfocus();

    connection = await _check();

    if (connection == false) {
      final appAlertDialog = AppAlertDialog();

      appAlertDialog.showAsync(
        context: context,
        status: AppAlertDialogStatus.error,
        message: "No Internet Connection!",
      );
    } else {
      // Validate form and save inputs to form model.
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();

        // Set button text to Loading during API call process.
        setState(() {
          _buttonText = "Loading";
          _isButtonDisabled = true;
        });

        // Call API.
        var alertDialogStatus = AppAlertDialogStatus.error;
        var alertDialogMessage = '';

        try {
          final repo = AuthRepo();
          final apiResponse = await repo.signInAsync(
            email: _formModel.email,
            password: _formModel.password,
          );

          // Login success, store user credentials into shared preference.
          final sharedPref = await SharedPreferences.getInstance();

          await sharedPref.setString(
              SharedPreferenceKeys.userEmail, _formModel.email);
          await sharedPref.setString(
              SharedPreferenceKeys.userToken, apiResponse.data.token);

          // Navigate to home screen.
          Navigator.of(context)
              .pushNamedAndRemoveUntil(AppRouter.home, (route) => false);

          return;
        } catch (ex) {
          Fimber.e('d;;Error request sign in.', ex: ex);

          alertDialogMessage = '$ex';
        }

        if (alertDialogMessage.isNotEmpty) {
          // Has message, show alert dialog.
          final appAlertDialog = AppAlertDialog();

          appAlertDialog.showAsync(
            context: context,
            status: alertDialogStatus,
            message: alertDialogMessage,
          );
          // Set button text back to Login after API call process in case user still stay in the page after.
          setState(() {
            _buttonText = "Login";
            _isButtonDisabled = false;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                initialValue: 'eve.holt@reqres.in',
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                onSaved: (newValue) => _formModel.email = newValue,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                decoration: InputDecoration(
                  hintText: 'Email',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              TextFormField(
                initialValue: 'cityslicka',
                textInputAction: TextInputAction.done,
                onSaved: (newValue) => _formModel.password = newValue,
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                //Hide password
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Password',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: _isButtonDisabled ? null : _onSubmitFormAsync,
                  child: Text("$_buttonText"),
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              SizedBox(
                height: appConst.kDefaultPadding,
                child: Text.rich(
                  TextSpan(
                    text: "Terms of Use",
                    style: TextStyle(color: Colors.blue),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        _onTextPressed();
                      },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
