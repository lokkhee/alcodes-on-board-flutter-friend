import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart';
import 'package:alcodes_on_board_flutter/models/view_models/friend_list_model.dart';
import 'package:flutter/material.dart';

class MyFriendDetails extends StatefulWidget {
  final FriendListModel friendList;

  MyFriendDetails({Key key, @required this.friendList}) : super(key: key);

  @override
  _MyFriendDetailsState createState() => _MyFriendDetailsState();
}

class _MyFriendDetailsState extends State<MyFriendDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Friend Details'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              width: double.infinity,
              color: kAccentColor,
              child: Column(
                children: [
                  Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image:
                            NetworkImage(widget.friendList.avatar.toString()),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            _content("First Name", widget.friendList.firstName),
            _content("Last Name", widget.friendList.lastName),
            _content("Email", widget.friendList.email),
          ],
        ),
      ),
    );
  }

  Widget _content(String upperText, String lowerText) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[100]), color: Colors.blue[100]),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            upperText,
            textScaleFactor: 1.0,
            textAlign: TextAlign.start,
          ),
          Text(lowerText, textScaleFactor: 1.8, textAlign: TextAlign.start),
        ],
      ),
    );
  }
}
